import React, { Component } from 'react';
import { AppRegistry, Text, StyleSheet, View, Dimensions } from 'react-native';

import { Card } from 'react-native-elements';

import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph
} from 'react-native-chart-kit';


export default class ChartsV extends Component{
    constructor(props){
        super(props);
        this.state = {
            message: this.props.message
        }
    }

    render() {
        return(
            <Card style={styles.cardStyle}>
                <Text style={styles.cardTitle}>{this.state.message}</Text>
                <View>
                    <LineChart
                        data={{
                            labels: ['January', 'February', 'March', 'April', 'May', 'June'],
                            datasets: [{
                                data: [
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100
                                ]
                            }]
                        }}
                        width={Dimensions.get('window').width} // from react-native
                        height={220}
                        chartConfig={{
                            backgroundColor: '#e26a00',
                            backgroundGradientFrom: '#fb8c00',
                            backgroundGradientTo: '#ffa726',
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                </View>
            </Card>
        );
    }
}


const chartConfig = {
    backgroundGradientFrom: '#1E2923',
    backgroundGradientTo: '#08130D',
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`
};


// Cards styles
const styles = StyleSheet.create({
    cardStyle: {
      marginBottom: 20,
    },
    cardTitle: {
        color: 'red',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomColor: '#e3e3e3',
        borderBottomWidth: 1,
    }
});


AppRegistry.registerComponent('ChartsV', () => myapp);