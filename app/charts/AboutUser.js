import React, { Component } from 'react';
import { AppRegistry, Text, StyleSheet, View } from 'react-native';

import { Card, CheckBox  } from 'react-native-elements';

export default class AboutUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            message: this.props.message,
            nameSurname: 'Agahuseyn Balabeyov',
            checked: false
        }
    }

    render() {
        const { tag } = this.props;
        const { checked } = this.state;

        return(
            <Card style={styles.cardStyle}>
                <Text style={styles.cardTitle}>{this.state.message}</Text>
                <View style={styles.cardBody}>
                    <Text style={styles.userNameStyle}>{this.state.nameSurname}</Text>
                    <Text style={styles.time}>1 s. 34 deq</Text>
                    <Text style={styles.mounthCount}>Ay erzinde 10 defe</Text>
                    <CheckBox
                        key={tag}
                        center
                        title={tag}
                        iconRight
                        iconType='material'
                        checkedIcon='clear'
                        uncheckedIcon='add'
                        checkedColor='red'
                        checked={checked}
                        onPress={() => this.setState({checked: !checked})}
                    />
                </View>
            </Card>
        );
    }
}





// Cards styles
const styles = StyleSheet.create({
    cardStyle: {
        marginBottom: 20,
    },
    userNameStyle: {
      color: '#000000',
      fontSize: 28,
      marginTop: 10,
        marginBottom: 10,
    },
    time: {
      color: '#e5e5e5',
        marginBottom: 5,
        fontSize: 22,
    },
    mounthCount: {
      color: 'red',
        fontSize: 18,
        marginBottom: 15,
    },
    cardTitle: {
        color: '#222222',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomColor: '#e3e3e3',
        borderBottomWidth: 1,
    }
});


AppRegistry.registerComponent('AboutUser', () => myapp);