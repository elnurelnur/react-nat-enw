import React, { Component } from 'react';
import { AppRegistry, Text, StyleSheet, View, Dimensions } from 'react-native';

import { Card } from 'react-native-elements';
import { Icon } from 'native-base';




export default class Component2 extends Component{
    constructor(props){
        super(props);
        this.state = {
            message: this.props.message,
            name: this.props.name,
            text: this.props.text
        }
    }

    render() {
        return(
            <Card style={styles.cardStyle}>
                <Text style={styles.cardTitle}>{this.state.message}</Text>
                <View style={styles.cardBody}>
                    <Text style={styles.cardCount}>{this.props.text}</Text>
                    <Text>
                        <Icon style={styles.iconStyle} name={this.state.name}/>
                    </Text>
                </View>
            </Card>
        );
    }
}


const chartConfig = {
    backgroundGradientFrom: '#1E2923',
    backgroundGradientTo: '#08130D',
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`
};


// Cards styles
const styles = StyleSheet.create({
    cardStyle: {
        marginBottom: 20,
        borderRadius: 4
    },
    cardBody: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        marginLeft: 15,
        marginRight: 15,
    },
    cardCount: {
      color: '#000000',
      fontSize: 28,
        fontWeight: 'bold'
    },
    iconStyle: {
        color: '#e5e5e5',
    },
    cardTitle: {
        color: '#222222',
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomColor: '#e3e3e3',
        borderBottomWidth: 1,
    }
});


AppRegistry.registerComponent('Component2', () => myapp);