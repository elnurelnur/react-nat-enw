import React, { Component } from 'react';
import { AppRegistry, ScrollView } from 'react-native';


import ChartsV from '../charts/ChartsV';
import AboutUser from '../charts/AboutUser';
import Component2 from '../Component2';

import MaterialTabs from 'react-native-material-tabs';


export default class Main extends Component{
    state = {
        selectedTab: 0,
    };


    render() {
        return(
            <ScrollView>
                <MaterialTabs
                    items={['Hamisi', 'Davamiyyet', 'Hamisi', 'Xercler', 'Diger']}
                    selectedIndex={this.state.selectedTab}
                    onChange={index => this.setState({ selectedTab: index })}
                    barColor='green'
                />
                <ChartsV message="Gecikenlerin sayi" />
                <AboutUser message="En cox geciken" />
                <Component2 message="Daxil olan senedler" text="575" name="archive" />
                <Component2 message="Xaric olan senedler" text="300" name="paper-plane" />
            </ScrollView>
        );
    }
}



AppRegistry.registerComponent('Main', () => myapp);