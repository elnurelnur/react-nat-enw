import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Header } from 'react-native-elements';




class Header1 extends Component{
    render() {
        return(
            <Header
                style={styles.headerStyle}
                leftComponent={{ icon: 'menu', color: '#fff' }}
                centerComponent={{ text: 'Statistika', style: { color: '#fff' } }}
                rightComponent={{ icon: 'home', color: '#fff' }}
            />
        );
    }
}


const styles = StyleSheet.create({
   headerStyle: {
       height: 50,
   }
});


export default Header1;