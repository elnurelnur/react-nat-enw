import React, { Component } from 'react';
import { AppRegistry, View, ScrollView } from 'react-native';
import { Container } from 'native-base';

import Header1 from './app/header/Index';
import Main from './app/main/index';


export default class myapp extends Component{

    render() {
        return(
            <Container style={{ paddingBottom: 20 }}>
                <View>
                    <Header1 style={{ backgroundColor: 'green' }} />
                </View>
                <ScrollView>
                    <Main/>
                </ScrollView>
            </Container>
        );
    }
}



AppRegistry.registerComponent('myapp', () => myapp);